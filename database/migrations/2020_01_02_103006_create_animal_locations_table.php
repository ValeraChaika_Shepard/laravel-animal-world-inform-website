<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimalLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animal_locations', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('animal_id', false, true)->nullable();
            $table->foreign('animal_id')->references('id')->on('animals');

            $table->bigInteger('country_id', false, true)->nullable();
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_locations');
    }
}
