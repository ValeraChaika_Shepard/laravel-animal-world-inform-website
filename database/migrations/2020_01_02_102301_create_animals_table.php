<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');

            $table->bigInteger('type_of_food_id', false, true)->nullable();
            $table->foreign('type_of_food_id')->references('id')->on('types_of_food');

            $table->bigInteger('type_of_domestication_id', false, true)->nullable();
            $table->foreign('type_of_domestication_id')->references('id')->on('types_of_domestication');

            $table->bigInteger('animal_class_id', false, true)->nullable();
            $table->foreign('animal_class_id')->references('id')->on('animal_classes');

            $table->text('description')->nullable();
            $table->text('photo_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
