<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $fillable = ['short_name', 'long_name'];

    public function animal_locations()
    {
        return $this->hasMany(Animal_location::class);
    }
}
