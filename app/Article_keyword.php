<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article_keyword extends Model
{
    protected $table = 'article_keywords';
    protected $fillable = ['article_id', 'keyword'];
    public $timestamps = false;

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
