<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type_of_domestication extends Model
{
    protected $table = 'types_of_domestication';
    protected $fillable = ['short_name', 'long_name'];

    public function animals()
    {
        return $this->hasMany(Animal::class);
    }
}
