<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';
    protected $fillable = ['title', 'short_text', 'main_text', 'create_date' , 'animal_id', 'author_id'];
    public $timestamps = false;

    public function article_keywords()
    {
        return $this->hasMany(Article_keyword::class);
    }

    public function animal()
    {
        return $this->belongsTo(Animal::class);
    }

    public function author()
    {
        return $this->belongsTo( Author::class);
    }
}
