<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    protected $table = 'animals';
    protected $fillable = ['name', 'type_of_food_id', 'type_of_domestication_id', 'animal_class_id', 'description', 'photo_link'];
    public $timestamps = false;

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function animal_locations()
    {
        return $this->hasMany(Animal_location::class);
    }

    public function type_of_food()
    {
        return $this->belongsTo(Type_of_food::class);
    }

    public function type_of_domestication()
    {
        return $this->belongsTo(Type_of_domestication::class);
    }

    public function animal_class()
    {
        return $this->belongsTo(Animal_class::class);
    }
}
