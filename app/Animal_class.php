<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animal_class extends Model
{
    protected $table = 'animal_classes';
    protected $fillable = ['short_name', 'long_name'];

    public function animals()
    {
        return $this->hasMany(Animal::class);
    }
}
