<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animal_location extends Model
{
    protected $table = 'animal_locations';
    protected $fillable = ['animal_id', 'country_id'];
    public $timestamps = false;

    public function animal()
    {
        return $this->belongsTo(Animal::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
