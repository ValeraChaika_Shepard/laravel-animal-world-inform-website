<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $table = 'users';
    protected $fillable = ['name', 'email', 'is_admin', 'password'];

    public function articles()
    {
        return $this->belongsTo( Article::class);
    }
}
