<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;

use App\Article_keyword;
use App\Article;
use App\Animal;
use http\Env\Request;

class ArticleController extends Controller
{
//    Вывести последние 5 статей (по дате написания)
//ПРОИСХОДИТ:
// Когда заходишь на сайт
    public function indexAction()
    {
        $articles = Article::join('users', 'users.id', '=', 'articles.author_id')
            ->select('articles.id as id', 'title', 'create_date', 'short_text')
            ->limit(5)->orderBy('create_date', 'desc')->get();
        return view('manyArticles', compact('articles'));
    }

//    Отобразить статью по ee id
//ПРОИСХОДИТ
// Когда "Читать полностью" или в поиске(лупа) вбить полное ИМЯ статьи
    public function displayOneArticleAction($id)
    {
        $article = Article::join('users', 'users.id', '=', 'articles.author_id')
            ->select('articles.id as id', 'title', 'main_text', 'create_date', 'animal_id', 'author_id', 'name')
            ->where('articles.id', $id)->get();

        $keywords = Article_keyword::where('article_id', $article[0]['id'])->select('keyword')->get();
        $keywords_str = "";
        for ($i = 0; $i < count($keywords); $i++) {
            $keywords_str .= $keywords[$i]->keyword . ";";
        }

        $animal = Animal::find($article[0]['animal_id']);
        $animal_name = "";
        if ($animal == null)
            $animal_name .= "БЕЗ КОНКРЕТНОГО ЖИВОТНОГО";
        else
            $animal_name .= $animal['name'];

        return view('soloArticle', compact('article', 'keywords_str', 'animal_name'));
    }
}