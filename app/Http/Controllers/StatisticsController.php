<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;

use App\Country;
use App\Animal_location;

class StatisticsController extends Controller
{
//    Вывести статистику по странам
//ПРОИСХОДИТ:
// Когда нажимаешь пункт меню "Статистика"
    public function displayStatisticsAction()
    {

        $data = [];

        $counties = Country::all();
        for ($i = 0; $i < count($counties); $i++) {
            $county = $counties[$i];

            $name = $county->long_name;
            $total_amount = 0;
            $amount_of_trav = 0;
            $amount_of_plot = 0;
            $amount_of_vseyad = 0;

            $animal_locations_by_country = $county->animal_locations()->get();
            for ($j = 0; $j < count($animal_locations_by_country); $j++) {
                $total_amount++;

                $food = $animal_locations_by_country[$j]->animal()->first()->type_of_food->long_name;

                if ($food == "травоядное") {
                    $amount_of_trav++;
                } else if ($food == "плотоядное") {
                    $amount_of_plot++;
                } else {
                    $amount_of_vseyad++;
                }
            }

            $tmp = [
                "name" => $name,
                "total" => $total_amount,
                "travoyadnix" => $amount_of_trav,
                "percent_travoyadnix" => number_format(($amount_of_trav / $total_amount) * 100, 2, '.', ''),
                "plotoyadnix" => $amount_of_plot,
                "percent_plotoyadnix" => number_format(($amount_of_plot / $total_amount) * 100, 2, '.', ''),
                'vseyadnix' => $amount_of_vseyad,
                "percent_vseyadnix" => number_format(($amount_of_vseyad / $total_amount) * 100, 2, '.', ''),
            ];

            array_push($data, $tmp);
        }

        return view('statistics', compact('data'));
    }
}