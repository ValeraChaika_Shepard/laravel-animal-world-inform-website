<?php

namespace App\Http\Controllers;

use App\Article;
use App\Article_keyword;
use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Support\Facades\Auth;

use App\Animal_class;
use App\Type_of_food;
use App\Type_of_domestication;

use App\Animal;

use App\Country;
use App\Animal_location;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;

class AdminController extends Controller
{
//    Выводит страницу создания статьи
//ПРОИСХОДИТ
// Когда нажимаешь пункт меню "Добавить" -> "Статья" (У АДМИНА)
    public function displayCreateArticlePageAction()
    {
        if (Auth::guest() || Auth::user()->is_admin == 0) {
            return redirect()->route('homeurl');
        }

        $animals = Animal::all();
        return view('addArticle', compact('animals'));
    }

//Происходит по нажатию "Отправить" на странице создания статьи
    public function tryToAddArticle(Request $request)
    {
        $this->validate($request, [
            'ar-title' => 'required',
            'ar-keywords' => 'required',
            'ar-short' => 'required',
            'ar-long' => 'required'
        ]);

        $title = $request->get('ar-title');
        $animal_id = $request->get('ar-animal');
        $keywords_str = $request->get('ar-keywords');
        $short_text = $request->get('ar-short');
        $long_text = $request->get('ar-long');

        $created_date = Carbon::now();
        $author_id = Auth::user()->id;

        if ($animal_id == -1)
            $animal_id = NULL;


//'title', 'short_text', 'main_text', 'create_date' , 'animal_id', 'author_id'
        $article = new Article([
            'title' => $title,
            'short_text' => $short_text,
            'main_text' => $long_text,
            'create_date' => $created_date,
            'animal_id' => $animal_id,
            'author_id' => $author_id
        ]);

        $article->save();

        $id_article = $article->id;
        $keywords_arr = explode(";", $keywords_str);

        for ($i = 0; $i < count($keywords_arr); $i++) {
            $keyword = $keywords_arr[$i];

//            'article_id', 'keyword'
            $art_key = new Article_keyword([
                'article_id' => $id_article,
                'keyword' => $keyword,
            ]);
            $art_key->save();
        }

        return redirect('/admin/addArticle')->with('success',
            'Статья добавлена');
    }


//    Выводит страницу создания животного
//ПРОИСХОДИТ
// Когда нажимаешь пункт меню "Добавить" -> "Животное" (У АДМИНА)
    public function displayCreateAnimalPageAction()
    {
        if (Auth::guest() || Auth::user()->is_admin == 0) {
            return redirect()->route('homeurl');
        }

        $classes = Animal_class::all();
        $foods = Type_of_food::all();
        $doms = Type_of_domestication::all();

        $data = compact('classes', 'foods', 'doms');

        return view('addAnimal', $data);
    }

//Происходит по нажатию "Отправить" на странице создания животного
    public function tryToAddAnimal(Request $request)
    {
        $this->validate($request, [
            'an-name' => 'required',
            'an-class' => 'required',
            'an-dom' => 'required',
            'an-pit' => 'required',
            'an-photo' => 'required',
            'an-country' => 'required',
            'an-descr' => 'required'
        ]);

        $name = $request->get('an-name');
        $class_id = $request->get('an-class');
        $dom_id = $request->get('an-dom');
        $pit_id = $request->get('an-pit');
        $photo = $request->get('an-photo');
        $country_str = $request->get('an-country');
        $descr = $request->get('an-descr');


        $animal = new Animal([
            'name' => $name,
            'type_of_food_id' => $pit_id,
            'type_of_domestication_id' => $dom_id,
            'animal_class_id' => $class_id,
            'description' => $descr,
            'photo_link' => $photo
        ]);

        $animal->save();

        $id_animal = $animal->id;
        $countries_arr = explode(";", $country_str);

        for ($i = 0; $i < count($countries_arr); $i++) {
            $country_name = $countries_arr[$i];
            $ct = Country::where('long_name', $country_name)->orWhere('short_name', $country_name)->first();

            if ($ct == null)
                throw ValidationException::withMessages(['dateKey' => 'Страна ' . $country_name . ' была введена с ошибкой.\n
                Животное сохранено, но в таблицу animal_location были некорректно добавлены данные']);

            $ct_id = $ct->id;

            $animal_location = new Animal_location([
                'animal_id' => $id_animal,
                'country_id' => $ct_id,
            ]);
            $animal_location->save();
        }

        return redirect('/admin/addAnimal')->with('success',
            'Животное добавлено');
    }

}