<?php

namespace App\Http\Controllers;

use App\Animal;
use App\Article;
use App\Article_keyword;
use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

use Illuminate\Validation\ValidationException;

use Illuminate\Support\Facades\DB;

use App\Animal_class;
use App\Type_of_food;
use App\Type_of_domestication;
use App\Country;

class SearchController extends Controller
{
//    Выводит страницу поиска по параметрам
//ПРОИСХОДИТ
// Когда нажимаешь пункт меню "Расширенный поиск"
    public function displayParamToSearchAction()
    {
        $classes = Animal_class::all();
        $foods = Type_of_food::all();
        $doms = Type_of_domestication::all();
        $counties = Country::all();

        $data = compact('classes', 'foods', 'doms', 'counties');

        return view('searchByParam', $data);
    }

    //Происходит по нажатию "Поиск" на странице расширенного поиска
    public function makeQueryFromParamSearch(Request $request)
    {
        $food_id = $request->get('pitanie');
        $dom_id = $request->get('odom');
        $class_id = $request->get('clas');
        $country_id = $request->get('count');

//        select * from animals INNER JOIN animal_locations ON animals.id = animal_locations.animal_id where type_of_food_id = 1 and country_id = 1

        $query = 'select animals.id as id, name, type_of_food_id, type_of_domestication_id, animal_class_id, animal_locations.country_id as country_id';
        $query .= ' from animals INNER JOIN animal_locations ON animals.id = animal_locations.animal_id';
        if ($food_id != null)
            $query .= ' where type_of_food_id = ' . $food_id;
        if ($dom_id != null)
            $query .= ' and type_of_domestication_id = ' . $dom_id;
        if ($class_id != null)
            $query .= ' and animal_class_id = ' . $class_id;

        $query .= '  and country_id = ' . $country_id;

        $animals = DB::select($query);

        $an = [];

        for ($i = 0; $i < count($animals); $i++) {
            $tmp = Animal::where('id', $animals[$i]->id)->first();
            $tmp["food"] = Type_of_food::where('id', $tmp->type_of_food_id)->first()->long_name;
            $tmp["dom"] = Type_of_domestication::where('id', $tmp->type_of_domestication_id)->first()->long_name;
            //$animals[$i]['food'] = Animal::where('id',$animals[$i]->id)->first()->type_of_food->long_name;
            //$animals[$i]['dom'] = Animal::where('id',$animals[$i]->id)->first()->type_of_domestication->long_name;
            array_push($an, $tmp);
        }

//        var_dump($an);

        return view('manyAnimals', ['animals' => $an]);
    }

//    На основании принятой строки запроса отображает некую информацию
// Сперва по приоритету ИМЕНА ЖИВОТНЫХ
// Затем НАЗВАНИЯ СТАТЕЙ
// Далее КЛЮЧЕВЫЕ СЛОВА
//ПРОИСХОДИТ
// При
    public function quickSearch(Request $request)
    {
        $req_text = mb_strtolower($request->get('search_request'));
//        По приоритету ищем животных
        $animal = Animal::where('name', $req_text)->first();

        if ($animal == null) {
            //Ищем по имени статьи
            $article = Article::where('title', $req_text)->first();
            if ($article == null) {
//                Ищем по ключевым словам
                $keywords = Article_keyword::where('keyword', $req_text)->get();
                if (count($keywords) == 1) {
//                    Выдать 1 статью
                    return redirect()->route('articleById', ['id' => $keywords[0]->article_id]);
                } else if (count($keywords) > 1) {
//                   Выдать много статей
                    $articles = [];
                    for ($i = 0; $i < count($keywords); $i++) {
                        $art_id = $keywords[$i]->article_id;
                        $tmp = Article::where('id', $art_id)->first();
                        array_push($articles, $tmp);
                    }
                    return view('manyArticles', compact('articles'));
                } else {
//                    НЕТ РЕЗУЛЬТАТА
                    return view('manyArticles', ['articles' => []]);
                }
            } else {
                return redirect()->route('articleById', ['id' => $article->id]);
            }
        } else {
            return redirect()->route('animalById', ['id' => $animal->id]);
        }

    }


}