<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;

use App\Animal;
use App\Animal_location;

class AnimalController extends Controller
{
//    Выводит страницу c несколькими животными
//ПРОИСХОДИТ
// Когда нажимаешь пункт меню "Животные по классу" -> "рыбы" (например)
    public function displayManyAnimalsAction($id)
    {
        $animals = Animal::join('types_of_food', 'types_of_food.id', '=', 'animals.type_of_food_id')
            ->join('types_of_domestication', 'types_of_domestication.id', '=', 'animals.type_of_domestication_id')
            ->join('animal_classes', 'animal_classes.id', '=', 'animals.animal_class_id')
            ->where('animal_class_id', $id)
            ->select('animals.id as id', 'name',
                'types_of_food.long_name as food',
                'types_of_domestication.long_name as dom',
                'animal_classes.long_name as class', 'description', 'photo_link')->get();
        return view('manyAnimals', compact('animals'));
    }

//    Выводит страницу c животным по id
//ПРОИСХОДИТ
// Когда нажимаешь на имя животного из таблицы или при вводе ИМЕНИ животного в поиске
    public function displaySoloAnimalAction($id)
    {
        $animal = Animal::join('types_of_food', 'types_of_food.id', '=', 'animals.type_of_food_id')
            ->join('types_of_domestication', 'types_of_domestication.id', '=', 'animals.type_of_domestication_id')
            ->join('animal_classes', 'animal_classes.id', '=', 'animals.animal_class_id')
            ->where('animals.id', $id)
            ->select('animals.id as id', 'name',
                'types_of_food.long_name as food',
                'types_of_domestication.long_name as dom',
                'animal_classes.long_name as class', 'description', 'photo_link')->get();

        $countries = Animal_location::where('animal_id', $animal[0]['id'])
            ->join('countries', 'countries.id', '=', 'animal_locations.country_id')
            ->select('countries.long_name as country')->get();

        $countries_str = "";
        for ($i = 0; $i < count($countries); $i++) {
            $countries_str .= $countries[$i]->country . ";";
        }

        return view('soloAnimal', compact('animal', 'countries_str'));
    }
}