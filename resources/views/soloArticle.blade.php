@extends('layouts.main')

@section('adminPartOfMenu')
    @auth()
        @if(Auth::user()->is_admin == 1)
            @include('adminPartOfMenu')
        @endif
    @endauth
@endsection

@section('dopcss')
    <link rel="stylesheet" href="{{ asset('css/solo-article.css') }}">
@endsection

@section('content')
    <div class="solo-article">
        <div class="meta-data-wrapper">
            <p class="author-login">{{ $article[0]['name'] }}</p>
            <p class="created-date">{{ $article[0]['create_date'] }}</p>
        </div>
        <p class="title">{{ $article[0]['title'] }}</p>
        <p class="text">{{ $article[0]['main_text'] }}:</p>
        <p class="keywords">Ключевые слова: <span>{{ $keywords_str }}</span></p>
        <p class="about-animal">Посвящено животному: <span>{{ $animal_name }}</span></p>
    </div>
@endsection