@extends('layouts.main')

@section('adminPartOfMenu')
    @auth()
        @if(Auth::user()->is_admin == 1)
            @include('adminPartOfMenu')
        @endif
    @endauth
@endsection

@section('dopcss')
    <link rel="stylesheet" href="{{ asset('css/many-animals-table.css') }}">
@endsection

@section('content')
    <div class="many-animals-table">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Наименование</th>
                <th scope="col">Тип питания</th>
                <th scope="col">Одомашненность</th>
            </tr>
            </thead>
            <tbody>

            @if(count($animals)>0)
            @foreach($animals as $key => $animal)
            <tr>
                <th scope="row">{{ $key+1 }}</th>
                <td><a href="{{ url('animal/' . $animals[$key]->id) }}">{{ $animals[$key]->name }}</a></td>
                <td>{{ $animals[$key]->food }}</td>
                <td>{{ $animals[$key]->dom }}</td>
            </tr>
            @endforeach
            @else
                <h4 class="text-center mt-5">Нет животных по заданным параметрам</h4>
            @endif

            </tbody>
        </table>
    </div>
@endsection