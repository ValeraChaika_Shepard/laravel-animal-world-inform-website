@extends('layouts.main')

@section('adminPartOfMenu')
    @auth()
        @if(Auth::user()->is_admin == 1)
            @include('adminPartOfMenu')
        @endif
    @endauth
@endsection

@section('dopcss')
    <link rel="stylesheet" href="{{ asset('css/staticstic.css') }}">
@endsection

@section('content')
    <div class="statictic">
        @foreach($data as $key => $tmp)
            <div class="country">
                <p class="country-name">{{ $tmp['name'] }}</p>
                <p>Количество травоядных: <span class="travo-kol">{{ $tmp['travoyadnix'] }} : {{  $tmp['percent_travoyadnix'] }}%</span></p>
                <p>Количество плотоядных: <span class="ploto-kol">{{ $tmp['plotoyadnix'] }} : {{  $tmp['percent_plotoyadnix'] }}%</span></p>
                <p>Количество всеядных: <span class="vseyad-kol">{{ $tmp['vseyadnix'] }} : {{ $tmp['percent_vseyadnix'] }}%</span></p>
                <p>Всего животных в БД сайта:<span>{{ $tmp['total'] }}</span></p>
            </div>
        @endforeach

        {{--<div class="country">--}}
            {{--<p class="country-name">Соединенные Штаты Америки</p>--}}
            {{--<p>Количество травоядных: <span class="travo-kol">30%</span></p>--}}
            {{--<p>Количество плотоядных: <span class="ploto-kol">50%</span></p>--}}
            {{--<p>Количество всеядных: <span class="vseyad-kol">20%</span></p>--}}
            {{--<p>Всего животных в БД сайта:<span>108</span></p>--}}
        {{--</div>--}}
    </div>
@endsection