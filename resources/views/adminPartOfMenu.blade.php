<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Добавить
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
        <a class="dropdown-item" href="{{ url('admin/addAnimal') }}">Животное</a>
        <a class="dropdown-item" href="{{ url('admin/addArticle') }}">Статью</a>
    </div>
</li>