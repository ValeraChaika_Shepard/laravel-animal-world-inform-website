@extends('layouts.main')

@section('adminPartOfMenu')
    @auth()
        @if(Auth::user()->is_admin == 1)
            @include('adminPartOfMenu')
        @endif
    @endauth
@endsection

@section('dopcss')
    <link rel="stylesheet" href="{{ asset('css/add-info.css') }}">
@endsection

@section('content')
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(\Session::has('success'))
        <div class="alert alert-success">
            <p>{{\Session::get('success')}}</p>
        </div>
    @endif

    <div class="add-wrapper">
        <p class="text-center">Добавление статьи</p>
        <form action="{{ route('trySaveArticle') }}">

            <div class="add-info-wrapper">
                <label for="ar-title-id">Введите заголовок:</label>
                <input type="text" name="ar-title" id="ar-title-id">
            </div>

            <div class="add-info-wrapper">
                <label for="ar-animal-id">Выберите о каком животном:</label>
                <select id="ar-animal-id" name="ar-animal">
                    <option disabled selected value="-1"> -- Не конкретное -- </option>
                    @foreach($animals as $animal)
                        <option value="{{ $animal->id }}">{{ $animal->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="add-info-wrapper">
                <label for="ar-keywords-id">Введите ключевые слова(;)</label>
                <input type="text" id="ar-keywords-id" name="ar-keywords">
            </div>


            <label for="ar-short-id">Введите краткое описание статьи:</label><br>
            <textarea id="ar-short-id" name="ar-short"></textarea>
            <br>

            <label for="ar-long-id">Введите полный текст статьи:</label><br>
            <textarea id="ar-long-id" name="ar-long"></textarea>
            <br>

            <input type="submit" class="btn btn-dark" value="Отправить">
        </form>
    </div>
@endsection