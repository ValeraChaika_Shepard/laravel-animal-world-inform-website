@extends('layouts.main')

@section('adminPartOfMenu')
    @auth()
        @if(Auth::user()->is_admin == 1)
            @include('adminPartOfMenu')
        @endif
    @endauth
@endsection

@section('dopcss')
    <link rel="stylesheet" href="{{ asset('css/big-search.css') }}">
@endsection

@section('content')
    <div class="search-wrapper">
        <form action="{{ route('makeSearchByParam') }}">
            <p class="text-center">Тип питания:</p>
            @foreach($foods as $key => $food)
                @if($key == 0)
                    <input name="pitanie" id="pit{{ $key+1 }}" type="radio" checked value={{ $foods[$key]->id }}>
                    <label for="pit{{ $key+1 }}">{{ $foods[$key]->long_name }}</label><br>
                @else
                    <input name="pitanie" id="pit{{ $key+1 }}" type="radio" value={{ $foods[$key]->id }}>
                    <label for="pit{{ $key+1 }}">{{ $foods[$key]->long_name }}</label><br>
                @endif
            @endforeach
            <hr>

            <p class="text-center">Одомашненность:</p>
            @foreach($doms as $key => $dom)
                <input name="odom" id="dom{{ $key+1 }}" type="radio" value={{ $doms[$key]->id }}>
                <label for="dom{{ $key+1 }}">{{ $doms[$key]->long_name }}</label><br>
            @endforeach
            <hr>

            <p class="text-center">Класс:</p>
            @foreach($classes as $key => $class)
                <input name="clas" id="clas{{ $key+1 }}" type="radio" value={{ $classes[$key]->id }}>
                <label for="clas{{ $key+1 }}">{{ $classes[$key]->long_name }}</label><br>
            @endforeach
            <hr>

            <p class="text-center">Страна обитания:</p>
            @foreach($counties as $key => $county)
                @if($key == 0)
                    <input name="count" id="count{{ $key+1 }}-id" type="radio" checked value={{ $counties[$key]->id }}>
                    <label for="count{{ $key+1 }}-id">{{ $counties[$key]->long_name }}</label><br>
                @else
                    <input name="count" id="count{{ $key+1 }}-id" type="radio" value={{ $counties[$key]->id }}>
                    <label for="count{{ $key+1 }}-id">{{ $counties[$key]->long_name }}</label><br>
                @endif
            @endforeach

            <input type="submit" value="Поиск" class="btn btn-dark">
        </form>
    </div>
@endsection