<?php
use App\User;
?>

@extends('layouts.main')

@section('adminPartOfMenu')
    @auth()
        @if(Auth::user()->is_admin == 1)
            @include('adminPartOfMenu')
        @endif
    @endauth
@endsection

@section('content')
    @if(count($articles)>0)
    @foreach($articles as $key => $article)
        <div class="article">
            <p class="title-article"><a href="{{ url('article/' . $articles[$key]->id) }}">{{ $articles[$key]->title }}</a><span>{{ $articles[$key]->create_date }}</span></p>
            <p>{{ $articles[$key]->short_text }}</p>
            <div class="read-all-wrapper">
                <a href="{{ url('article/' . $articles[$key]->id) }}" class="read-all">Читать полностью</a>
            </div>
        </div>
    @endforeach
    @else
        <h4 class="text-center mt-5">Поиск не дал результата!</h4>
    @endif

    {{--<div class="article">--}}
        {{--<p class="title-article"><a href="{{ url('article/1') }}">Название статьи 1</a><span>2020-01-01 12:20:00</span></p>--}}
        {{--<p>--Короткий текст--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, quidem distinctio! Recusandae accusantium laborum officia et, possimus molestiae, quibusdam eveniet libero temporibus odit ipsam dicta velit cupiditate dolorum aliquid. At optio magni itaque! Perspiciatis natus amet alias ratione provident beatae. Hic temporibus minus reiciendis ab enim, sequi rem ducimus velit, officiis laudantium blanditiis. Eius sed velit assumenda, officia porro labore sint pariatur sapiente! Deserunt, facere. Adipisci ex eum dicta cum tempora dolorem possimus praesentium mollitia nulla vero, expedita provident aliquid perspiciatis obcaecati facere dolorum, porro sed ab fugit maiores nesciunt. Facilis ipsa dolores eum cupiditate sunt harum! Laborum nostrum, quae.</p>--}}
        {{--<div class="read-all-wrapper">--}}
            {{--<a href="{{ url('article/1') }}" class="read-all">Читать полностью</a>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection