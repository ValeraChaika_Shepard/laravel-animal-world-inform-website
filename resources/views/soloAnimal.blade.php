@extends('layouts.main')

@section('adminPartOfMenu')
    @auth()
        @if(Auth::user()->is_admin == 1)
            @include('adminPartOfMenu')
        @endif
    @endauth
@endsection

@section('dopcss')
    <link rel="stylesheet" href="{{ asset('css/animal-wrapper.css') }}">
@endsection

@section('content')
    <div class="animal-wrapper">

        <div class="short-info-wrapper">
            <div class="photo-wrapper">
                <img src="{{ $animal[0]->photo_link }}" alt="">
            </div>

            <div class="info-wrapper">
                <p class="name">{{ $animal[0]->name }}</p>
                <p class="type_of_food">Питание: <span>{{ $animal[0]->food }}</span></p>
                <p class="type_of_domestication">Одомашненность: <span>{{ $animal[0]->dom }}</span></p>
                <p class="animal_class">Класс: <span>{{ $animal[0]->class }}</span></p>
                <p class="animal_locations">Страны обитания: <span>{{ $countries_str }}</span></p>
            </div>
        </div>

        <div class="description-wrapper">
            <p>{{ $animal[0]->description }}</p>
        </div>
    </div>
@endsection