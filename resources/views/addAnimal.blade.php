@extends('layouts.main')

@section('adminPartOfMenu')
    @auth()
        @if(Auth::user()->is_admin == 1)
            @include('adminPartOfMenu')
        @endif
    @endauth
@endsection

@section('dopcss')
    <link rel="stylesheet" href="{{ asset('css/add-info.css') }}">
@endsection

@section('content')
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(\Session::has('success'))
        <div class="alert alert-success">
            <p>{{\Session::get('success')}}</p>
        </div>
    @endif

    <div class="add-wrapper">
        <p class="text-center">Добавление животного</p>
        <form action="{{ route('trySaveAnimal') }}">

            <div class="add-info-wrapper">
                <label for="an-name-id">Введите имя:</label>
                <input type="text" id="an-name-id" name="an-name">
            </div>

            <div class="add-info-wrapper">
                <label for="an-class-id">Выберите класс животного:</label>
                <select id="an-class-id" name="an-class">
                    @foreach($classes as $class)
                        <option value="{{ $class->id }}">{{ $class->long_name }}</option>
                    @endforeach
                </select>
            </div>


            <div class="add-info-wrapper">
                <label for="an-dom-id">Выберите тип одомашненности:</label>
                <select id="an-dom-id" name="an-dom">
                    @foreach($doms as $dom)
                        <option value="{{ $dom->id }}">{{ $dom->long_name }}</option>
                    @endforeach
                </select>
            </div>


            <div class="add-info-wrapper">
                <label for="an-pit-id">Выберите тип питания:</label>
                <select id="an-pit-id" name="an-pit">
                    @foreach($foods as $food)
                        <option value="{{ $food->id }}">{{ $food->long_name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="add-info-wrapper">
                <label for="an-photo-id">Введите url фото:</label>
                <input type="text" id="an-photo-id" name="an-photo">
            </div>

            <div class="add-info-wrapper">
                <label for="an-country-id">Введите страны проживания(;)</label>
                <input type="text" id="an-country-id" name="an-country">
            </div>

            <label for="an-descr-id">Введите описание животного:</label><br>
            <textarea id="an-descr-id" name="an-descr"></textarea>
            <br>

            <input type="submit" class="btn btn-dark" value="Отправить">
        </form>
    </div>
@endsection