<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

//Стартовая страница
Route::get('/', 'ArticleController@indexAction')->name('homeurl');

//Страница: Одна статья по указанному id
Route::get('/article/{id}', 'ArticleController@displayOneArticleAction')->name('articleById');

//Страница: Статистику стран
Route::get('/statistics', 'StatisticsController@displayStatisticsAction');

//Страница: Страница расширенного поиск
Route::get('/search-by-params', 'SearchController@displayParamToSearchAction');

//Страница: Животные по классу (id класса)
Route::get('/animals/{id}', 'AnimalController@displayManyAnimalsAction');

//Страница: Животное по классу (id класса)
Route::get('/animal/{id}', 'AnimalController@displaySoloAnimalAction')->name('animalById');

//Страница: Добавление статьи
Route::get('/admin/addArticle', 'AdminController@displayCreateArticlePageAction');

//Страница: Добавление животного
Route::get('/admin/addAnimal', 'AdminController@displayCreateAnimalPageAction');


//Попытка сохранения животного
Route::any('tryToAddAnimal', 'AdminController@tryToAddAnimal')->name('trySaveAnimal');
Route::any('tryToAddArticle', 'AdminController@tryToAddArticle')->name('trySaveArticle');

//Попытка получения информации от быстрого запроса
Route::any('quickSearchRequest', 'SearchController@quickSearch')->name('quickSearch');

//Порытка получения информации от поиска по параметрам
Route::any('makeSearchByParamRequest', 'SearchController@makeQueryFromParamSearch')->name('makeSearchByParam');


//Route::get('/home', 'HomeController@index')->name('home');
